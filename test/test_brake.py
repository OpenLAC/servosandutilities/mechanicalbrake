import numpy as np
import matplotlib.pyplot as plt
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + '/../')

from utils.ci.dll_wrapper import Type2DllWrapper, find_dll
import pytest
import time


@pytest.fixture#(scope="module")
def dll():
    f = find_dll(os.path.dirname(__file__) + "/../", 'mech_brake')
    
    def init(dll_subroutine_init, dll_subroutine_update, arraysizes_init, arraysizes_update, init_array):
        return Type2DllWrapper(f, dll_subroutine_init, dll_subroutine_update, arraysizes_init, arraysizes_update, init_array)
    return init






def test_brake(dll):
    
    DELTAT = 0.01
    T = 50
    N_ITER = int(T // DELTAT)
    out = np.empty([N_ITER, 100])
    settings = [9360000,  # Fully deployed maximum brake torque [Nm] (0.6*max torque)
                100,  # Parameter alpha used in Q = tanh(omega*alpha), typically 1e2/Omega_nom
                5,  # deploy time (time to fully activate brake)
                2,  # delay time (time before brake activation starts
                ]
    
    
    brake = dll('init_mech_brake', 'update_mech_brake', (100, 1), (100, 100), settings)
    time = np.arange(N_ITER) * DELTAT
    speed = 1
    brake_signal = 0
    with brake:
        for i, t in enumerate(time):
            if t == 5:
                brake_signal = 1
            elif t == 25:
                brake_signal = 0
            elif t == 40:
                brake_signal = 1

            out[i, :] = brake.update([t, speed, brake_signal])
            speed += 0.0001
            speed -= out[i, 0] / 93600000 * DELTAT

    ref_torque = [0, 0, 0, 0, 0, 0, 0, 18719, 1890719, 3762719, 5634719, 7506720, 9360000, 9360000, 9360000, 9360000,
                  9360000, 9360000, 9360000, 9360000, 9359999, 9359872, 1052614, 936003, 936000, 936000, 936000,
                  934128, 908602, 889572, 843222, 691441, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18719, 1890719, 3762719,
                  5634094, 1774851, 954262, 936000, 936000]
    if 0:
        labels = ['brake torque', 'brakecommand', 'brakeactuator', 'omega']
        _, axes = plt.subplots(len(labels), sharex=True)
        for i, ax in enumerate(axes):
            ax.plot(time, out[:, i], label=labels[i])
            for t in [5, 25, 40]:
                m = time == t
                ax.plot(time[m], out[:, i][m], '.r')
            ax.legend(loc=1)
        # axes[0].plot(time[::100], ref_torque)
        plt.savefig('test_brake.png')
        plt.show()
        
    
    np.testing.assert_array_equal(out[::100, 0].astype(int), ref_torque)

def test_brake_instability(dll):
    
    DELTAT = 0.01
    T = 10
    N_ITER = int(T // DELTAT)
    out = np.empty([N_ITER, 100])
    settings = [9360000,  # Fully deployed maximum brake torque [Nm] (0.6*max torque)
                100,  # Parameter alpha used in Q = tanh(omega*alpha), typically 1e2/Omega_nom
                1,  # deploy time (time to fully activate brake)
                1,  # delay time (time before brake activation starts
                ]
    
    brake = dll('init_mech_brake', 'update_mech_brake', (100, 1), (100, 100), settings)
    time = np.arange(N_ITER) * DELTAT
    speed = 0
    last_speed = speed
    brake_signal = 0

    with brake:
        for i, t in enumerate(time):
            if t == 1:
                brake_signal = 1
            out[i, :] = brake.update([t, last_speed, brake_signal])
            last_speed = speed
            speed += 0.001
            speed -= out[i, 0] / 9360000 * DELTAT
            
       
    
    if 0:
        print (list(out[::100,2]))
        labels = ['brake torque', 'brakecommand', 'brakeactuator', 'omega']
        fig, axes = plt.subplots(len(labels), sharex=True)
        for i, ax in enumerate(axes):
            ax.plot(time, out[:, i], label=labels[i])
            for t in [1]:
                m = time == t
                ax.plot(time[m], out[:, i][m], '.r')
            ax.legend(loc=1)
        plt.savefig('test_brake_instability.png')
        plt.show()
        
    # check brake actuator
    np.testing.assert_array_almost_equal(out[::100,2], [0.0, 0.0, 0.01, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0])


if __name__ == '__main__':
    test_brake()
