# MechanicalBrake

[![pipeline status](https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/mechanicalbrake/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/OpenLAC/servosandutilities/mechanicalbrake/-/commits/master)

Type2dll used by HAWC2

```
begin dll;
    begin type2_dll;
      name	dtu_we_controller;
      filename	./control/dtu_we_controller.dll;
      ...
    end type2_dll;
;
    begin type2_dll;
      name	mech_brake;
      filename	./mech_brake.dll;
      dll_subroutine_init	init_mech_brake;
      dll_subroutine_update	update_mech_brake;
      arraysizes_init	100 1;
      arraysizes_update	100 100;
      begin init;
        constant	1 9360000;	Fully deployed maximum brake torque [Nm] (0.6*max torque)
        constant	2 100;	Parameter alpha used in Q = tanh(omega*alpha), typically 1e2/Omega_nom
        constant	3 0.5;	Delay time for before brake starts to deploy [s]
        constant	4 0.6;	Time for brake to become fully deployed [s]
      end init;
;
      begin output;
        general time;	Time [s]
        constraint bearing1	shaft_rot 1 only 2;	Generator LSS speed [rad/s]
        dll inpvec	1 25;	Command to deploy mechanical disc brake [0,1]
      end output;	
;
      begin actions;
        mbdy	moment_int shaft 1 3 shaft towertop 2;	Brake LSS torque [Nm]
      end actions;
    end type2_dll;
end dll;
```

## Examples from the tests:

### test_brake

- Brake delay time: 2s
- Brake deploy time: 5s

time | brake event
--- | ---
5 | activate
25 | release
40 | reactivate

![test_brake](test_brake.png)



### test_brake_instability

rotor speed of previous time step is passed to the brake which thereby results in a delayed unstable response

- Brake delay time: 1s
- Brake deploy time: 1s

time | brake event
--- | ---
1 | activate

![test_brake](test_brake_instability.png)
